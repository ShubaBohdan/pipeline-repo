package com.shuba.pipeline.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PipeResponse {

    private String resp;

}

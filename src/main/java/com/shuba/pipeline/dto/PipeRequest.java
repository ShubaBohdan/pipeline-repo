package com.shuba.pipeline.dto;

import lombok.Data;

@Data
public class PipeRequest {
    private String name;
}

package com.shuba.pipeline.service;

import com.shuba.pipeline.dto.PipeResponse;
import org.springframework.stereotype.Service;

@Service
public class PipeService {

    public PipeResponse sayHello() {
        return new PipeResponse("HI");
    }

    public PipeResponse yell(String name) {
        return new PipeResponse("AAAAAAAAYYYYYYYYY! " + name);
    }

}

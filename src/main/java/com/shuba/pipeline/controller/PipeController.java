package com.shuba.pipeline.controller;

import com.shuba.pipeline.dto.PipeRequest;
import com.shuba.pipeline.dto.PipeResponse;
import com.shuba.pipeline.service.PipeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class PipeController {

    private final PipeService pipeService;

    @GetMapping("/hello1")
    public PipeResponse sayHello () {
        return pipeService.sayHello();
    }

    @GetMapping("/yell")
    public PipeResponse yell (@RequestBody PipeRequest request) {
        return pipeService.yell(request.getName());
    }
}

package com.shuba.pipeline.service;

import com.shuba.pipeline.dto.PipeResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class PipeServiceTest {


    @Test
    public void test1() {
        PipeService pipeService = new PipeService();
        Assertions.assertNotNull(pipeService.sayHello());
    }

    @Test
    public void test2() {
        PipeService pipeService = new PipeService();
        PipeResponse pipeResponse = pipeService.sayHello();
        Assertions.assertEquals("HI",  pipeResponse.getResp());
    }
}
